<?php

namespace backend\modules\pressKit\models;

use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%press_kit}}".
 *
 * @property integer $id
 */
class PressKit extends ActiveRecord implements BackendModel
{

    /**
     * Attribute for imageUploader
     */
    public $file;

    /**
     * Temporary sign which used for saving images before model save
     * @var string
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%press_kit}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sign'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/press-kit', 'ID'),
            'file' => Yii::t('back/press-kit', 'File'),
            'link' => Yii::t('back/press-kit', 'Link'),
        ];
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/press-kit', 'Press Kit');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'link',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->getFileLink();
                        }
                    ],
                    // 'id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'link',
                        'format' => 'raw',
                        'value' => $this->getFileLink()
                    ],
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new PressKitSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'file' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'file',
                    'saveAttribute' => \common\models\PressKit::SAVE_ATTRIBUTE_FILE,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }


    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['attribute' => \common\models\PressKit::SAVE_ATTRIBUTE_FILE]);
    }

    public function getFileLink()
    {
        if ($file = $this->uploadedFile) {
            $fileModel = $file->file;
            $src = FPM::originalSrc($file->file_id);
            $name = $fileModel->base_name . '.' . $fileModel->extension;

            return Html::a($name, $src, ['target' => '_blank']);
        }
        return '';
    }
}
