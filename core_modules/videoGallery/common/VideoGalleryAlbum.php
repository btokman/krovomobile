<?php

namespace common\models;

use creocoder\translateable\TranslateableBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;

/**
 * This is the model class for table "{{%video_album}}".
 *
 * @property integer                 $id
 * @property integer                 $parent_id
 * @property string                         $label
 * @property string                         $alias
 * @property string                         $description
 * @property string                         $content
 * @property string                         $author
 * @property integer                        $published
 * @property integer                        $position
 * @property integer                        $created_at
 * @property integer                        $updated_at
 *
 * @property VideoGalleryAlbum              $parent
 * @property VideoGalleryAlbum[]            $videoAlbums
 * @property VideoGalleryAlbumTranslation[] $translations
 */
class VideoGalleryAlbum extends \common\components\model\ActiveRecord
{
    const VIDEO_ALBUM_IMAGE = 'video-album-image';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%video_album}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(VideoGalleryAlbumTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
            'content',
            'author',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class'                 => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo'           => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }
}
