<?php
namespace frontend\modules\currency\controllers;

use Yii;
use frontend\components\FrontendController;
use frontend\modules\currency\models\Currency;

/**
 * Class CurrencyController
 */
class CurrencyController extends FrontendController
{

    /**
     * Action to change current currency, stores it session and redirect to previous page
     *
     * @param string $currency `code` attribute
     *
     * @return \yii\web\Response
     */
    public function actionSetCurrency($currency)
    {
        $model = Currency::findOne(['code' => $currency]);
        if ($model !== null) {
            $session = Yii::$app->getSession();
            $session->set(Currency::SESSION_KEY, $model->id);
        }
        return $this->redirect(Yii::$app->getRequest()->getReferrer());
    }
}
