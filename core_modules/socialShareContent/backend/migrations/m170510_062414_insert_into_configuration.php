<?php

use backend\modules\configuration\models\Configuration;
use console\components\Migration;

/**
 * Class m170510_062414_insert_into_configuration migration
 */
class m170510_062414_insert_into_configuration extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $time = time();

        $this->insert($this->tableName, [
            'id' => 'default_share_image',
            'type' => Configuration::TYPE_STRING,
            'description' => 'Default image for social sharing',
            'value' => '',
            'created_at' => $time,
            'updated_at' => $time,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 'default_share_image']);
    }
}
