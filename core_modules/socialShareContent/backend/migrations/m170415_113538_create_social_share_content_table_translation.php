<?php

use console\components\Migration;

/**
 * Class m170415_113538_create_social_share_content_table_translation migration
 */
class m170415_113538_create_social_share_content_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%social_share_content_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%social_share_content}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'title' => $this->string()->comment('Title'),
                'description' => $this->text()->comment('Description'),
            ],
            $this->tableOptions
        );

        $this->addPrimaryKey('', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-s_s_c_translation-model_id-soc_share_content-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
