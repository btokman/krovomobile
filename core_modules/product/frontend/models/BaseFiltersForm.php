<?php
namespace frontend\modules\product\models;

use common\components\model\ActiveQuery;
use common\models\ProductEav;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: art
 * Date: 25.01.2017
 * Time: 13:04
 */
class BaseFiltersForm extends Model
{
    public $options;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['options'], 'safe'],
        ];
    }

    /**
     * @param ActiveQuery $query
     * @return mixed
     */
    public function setFilter($query)
    {
        if ((isset($this->options)) && (count($this->options) > 0)) {
            //select packing by filter
            $packingArrayQuery = ProductEav::find()
                ->select(['product_id', 'COUNT(`product_eav`.`product_id` ) as filter_count']);

            $queryOrArray = [];
            $queryOrArray[] = 'or';

            $uniqueOptions = [];
            foreach ($this->options as $option) {
                $optionArray = json_decode($option);

                if (!in_array($optionArray->id, $uniqueOptions)) {
                    $uniqueOptions[] = $optionArray->id;
                }

                $queryOrArray[] = [
                    '`product_eav`.`attribute_id`' => $optionArray->id,
                    'LOWER(`product_eav`.`value`)' => strtolower($optionArray->value)
                ];
            }

            $packingArrayQuery->andFilterWhere($queryOrArray);
            $packingArrayQuery->addGroupBy('product_eav.product_id');
            $packingArrayQuery->having('filter_count=' . count($uniqueOptions));

            $packingArrayData = $packingArrayQuery->asArray()->all();

            $packingArray = [];
            foreach ($packingArrayData as $item) {
                $packingArray[] = $item['product_id'];
            }

            $query->andWhere(['`product`.`id`' => $packingArray]);
        }

        return $query;
    }
}
