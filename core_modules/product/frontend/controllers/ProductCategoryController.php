<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 12.03.2017
 * Time: 21:17
 */
namespace frontend\modules\product\controllers;

use common\models\Product;
use common\models\ProductCategory;
use frontend\components\FrontendController;
use frontend\modules\product\assets\ProductAsset;
use frontend\modules\product\helpers\NestedSetHelper;
use frontend\modules\product\models\BaseFiltersForm;
use frontend\modules\product\Module;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class ProductCategoryController extends FrontendController
{

    public function actionIndex()
    {
        $roots = ProductCategory::find()->andWhere(['published' => 1])->orderBy('root, left')->roots()->all();
        $allCategories = ProductCategory::find()->where(['published' => 1])->orderBy('root, left')->all();

        $navXConfig = NestedSetHelper::buildNavXConfig($roots);
        $categoriesArray = NestedSetHelper::buildCategoryArray($roots);


        return $this->render(
            'index',
            [
                'allCategories' => $allCategories,
                'navXConfig' => $navXConfig,
                'categoriesArray' => $categoriesArray,
            ]
        );
    }

    /**
     * @param null $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($alias = null)
    {
        ProductAsset::register(\Yii::$app->view);
        $module = \Yii::$app->getModule('product');

        $model = ProductCategory::findSingleCategory($alias);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        MetaTagRegister::register($model);

        $postData = \Yii::$app->request->post();
        $baseFiltersForm = new BaseFiltersForm();

        //settings
        $perPage = 20;

        $query = Product::find()
            ->select(['`product`.*'])
            ->where([
                'published' => 1,
            ])
            ->leftJoin('product_to_category', '`product_to_category`.`product_id` = `product`.`id`')
            ->andWhere(['`product_to_category`.`category_id`' => $model->id]);

        $itemView = '_baseProductListView';
        if ($module->params['packingType'] == Module::PACKING_AS_OPTION) {
            $query->andWhere([
                'show_as_separate' => 1
            ]);

            $itemView = '_baseProductWithPackingListView';
        }

        //apply base filter
        //if base filter data send as POST
        if ($baseFiltersForm->load($postData) && $baseFiltersForm->validate()) {
            $query = $baseFiltersForm->setFilter($query);
        }

        //get eav filters
        $attributeFilters = $model->getAttributesValue();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $perPage,

            ],
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'attributeFilters' => $attributeFilters,
            'baseFiltersForm' => $baseFiltersForm,
            'itemView' => $itemView,
        ]);
    }
}
