<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 12.03.2017
 * Time: 21:21
 */
use frontend\modules\product\widgets\CategoryUlWidget;
use kartik\nav\NavX;
use yii\bootstrap\NavBar;


NavBar::begin();
echo NavX::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => $navXConfig,
    'activateParents' => true,
    'encodeLabels' => false
]);
NavBar::end();
?>

<div class="container-fluid">
  <div class="row">
      <div class="col-md-6">
          <h5>ul - li example</h5>
          <?php echo CategoryUlWidget::widget(['categoryList' => $allCategories]); ?>
      </div>
      <div class="col-md-6" >
          <h5>dump category array example</h5>
          <?php \yii\helpers\VarDumper::dump($categoriesArray,10,true); ?>

      </div>
  </div>
</div>



