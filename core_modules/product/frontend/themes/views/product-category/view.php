<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.03.2017
 * Time: 01:43
 */
use common\models\ProductCategory;
use yii\bootstrap\ActiveForm;

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1> <?= $model->label ?> </h1>
        </div>
        <div class="col-md-3">
            <?php
            //side bar filter
            $form = ActiveForm::begin([
                'action' => ProductCategory::getCategoryViewUrl(['alias' => $model->alias]),
                'id' => 'base-filters-form',
                'fieldConfig' => [
                    'options' => [
                        'tag' => false,
                    ],
                ],
            ]);
            ?>
            <?php foreach ($attributeFilters as $filterLabel => $filterData): ?>
                <b><?= $filterLabel ?></b>
                <ul>
                    <?php foreach ($filterData['values'] as $value => $displayValue):
                        $valueJson = json_encode([
                            'id' => strval($filterData['attribute_id']),
                            'value' => $value,
                        ]);
                        ?>
                        <li>
                            <?=
                            $form->field($baseFiltersForm, 'options[]')
                                ->checkbox([
                                    'id' => 'option' . Yii::$app->security->generateRandomString(5),
                                    'value' => $valueJson,
                                    'label' => $displayValue,
                                    'data-class' => 'base-filter',
                                    'uncheck' => null,
                                ]);
                            ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endforeach; ?>

            <?php
            ActiveForm::end();
            ?>

        </div>
        <div class="col-md-9">
            <?php
            //list of products on orders panel
            \yii\widgets\Pjax::begin([
                'id' => 'pjax-catalog-list',
                'timeout' => 25000,
                'linkSelector' => '.pjax_link, .pagination li a',
                'enablePushState' => true,
            ]);

            echo \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => $itemView,
            ]);

            \yii\widgets\Pjax::end();
            ?>
        </div>
    </div>
</div>