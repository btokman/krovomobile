$('input[data-class="base-filter"]').change(function() {

    var container = $('#pjax-catalog-list');
    var baseFiltersForm = $('#base-filters-form');
    var pjaxUrl = baseFiltersForm.attr('action');

    $.pjax({
        url: pjaxUrl,
        container: container,
        data: baseFiltersForm.serialize(),
        scrollTo: false,
        timeout: 15000,
        type: 'POST',
        push: false
    });

});
