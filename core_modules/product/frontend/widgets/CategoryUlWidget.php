<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.03.2017
 * Time: 01:02
 */

namespace frontend\modules\product\widgets;

use yii\base\Widget;

class CategoryUlWidget extends Widget
{
    public $categoryList;

    /**
     * @return string
     */
    public function run()
    {

        return $this->render('categoryUlWidgetView', [
            'models' => $this->categoryList,
        ]);
    }
}
