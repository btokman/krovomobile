<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_storehouse_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $address
 * @property string $work_time
 * @property string $short_desc
 * @property string $content
 */
class ProductStorehouseTranslation extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_storehouse_translation}}';
    }
}
