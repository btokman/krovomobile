<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%product_storehouse}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $address
 * @property string $work_time
 * @property double $lat
 * @property double $lng
 * @property string $short_desc
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductStorehouseTranslation[] $translations
 */
class ProductStorehouse extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_storehouse}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'address',
            'work_time',
            'short_desc',
            'content',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductStorehouseTranslation::className(), ['model_id' => 'id']);
    }
}
