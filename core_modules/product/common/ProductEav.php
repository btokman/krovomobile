<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%product_eav}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $attribute_id
 * @property string $value
 *
 * @property ProductAttribute $attribute0
 * @property Product $product
 * @property ProductEavTranslation[] $translations
 */
class ProductEav extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_eav}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'value',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasOne(ProductAttribute::className(), ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductEavTranslation::className(), ['model_id' => 'id']);
    }
}
