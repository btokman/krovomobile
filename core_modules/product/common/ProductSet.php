<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_set}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $product_set_id
 * @property double $discount
 * @property string $price
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Product $product
 * @property Product $productSet
 */
class ProductSet extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_set}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSet()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_set_id']);
    }
}
