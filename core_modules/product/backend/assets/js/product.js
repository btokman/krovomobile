/**
 * Created by air on 3/9/17.
 */
//--------- map js code -----------
$(".product_map").each(function () {
    initMap($(this).attr('id'));
});

function initMap(mapId) {
    if (mapId == undefined) {
        var lastMap = $('.product_map').last();
        var lastMapId = lastMap.attr('id');
    }
    else {
        lastMapId = mapId;
    }

    var uniqueId = lastMapId.replace('product_map_', '');

    var nearestLatInput = $('#product_map_lat_' + uniqueId);
    var nearestLongInput = $('#product_map_lng_' + uniqueId);

    var coordsFromBaseLat = parseFloat(nearestLatInput.val());
    var coordsFromBaseLon = parseFloat(nearestLongInput.val());
    var coordsFromBase = {lat: coordsFromBaseLat, lng: coordsFromBaseLon};

    var map = new google.maps.Map(document.getElementById(lastMapId), {
        zoom: 7,
        elementId: lastMapId,
        center: coordsFromBase,
        scrollwheel: false,
        styles: [{
            stylers: [{
                saturation: -100
            }]
        }]
    });

    var markers = [
        new google.maps.Marker({
            position: coordsFromBase,
            map: map
        })
    ];

    //try to get coordinates with stackoverflow
    google.maps.event.addListener(map, "click", function (event) {

        var lat = event.latLng.lat();
        var lng = event.latLng.lng();
        // populate yor box/field with lat, lng

        $('#product_map_lat_' + uniqueId).val(lat);
        $('#product_map_lng_' + uniqueId).val(lng);

        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }

        mark = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map
        });
        markers.push(mark);
    });
}

//--------- one to many widget -----------

$(document).on('click', '.add-entity-btn', function (event) {
    var $tr = $(this).closest('.tr_clone');
    var $clone = $tr.clone();
    $clone.find(':text').val('');
    $tr.after($clone);

    $clone.find('input').val('');
    $clone.find('.remove-entity-btn').css('visibility', 'visible');

});

$(document).on('click', '.remove-entity-btn', function (event) {
    var $tr = $(this).closest('.tr_clone');
    $tr.remove();
});


//--------- product attribute page -----------
$(document).ready(function () {
    var productAttributeTypeSelect = $("select[data-type='product_attribute_type_select']");
    var hideOptionsValue = productAttributeTypeSelect.attr('data-hide-options');
    var value = productAttributeTypeSelect.val();
    var optionsBlockClass = productAttributeTypeSelect.attr('data-options-block');

    if (value == hideOptionsValue) {
        $('.' + optionsBlockClass).hide();
    }
    else {
        $('.' + optionsBlockClass).show();
    }

});


$(document).on("change", "select[data-type='product_attribute_type_select']", function () {

    var value = $(this).val();
    var hideOptionsValue = $(this).attr('data-hide-options');
    var optionsBlockClass = $(this).attr('data-options-block');
    if (value == hideOptionsValue) {
        $('.' + optionsBlockClass).hide();
    }
    else {
        $('.' + optionsBlockClass).show();
    }

});

//--------- product page -----------

function updateAttributes(selectedGroups, selectAttributeId) {
    var attributeSelect = $('#'+selectAttributeId);
    var attributesUrl = attributeSelect.attr('data-attributes-by-group-url');

    if (selectedGroups == null) {
        attributeSelect.val([]);
    }

    //get group attributes value
    $.ajax({
        method: "POST",
        url: attributesUrl,
        data: {groupIds: selectedGroups}
    })
        .done(function (attributes) {
            var attributesObjsArray = JSON.parse(attributes);

            $.each(attributesObjsArray, function (index, value) {
                addSelect2Values(attributeSelect, index, value);

            });

            attributeSelect.trigger('change');
        });

}

function addSelect2Values(select2element, newKey, newValue) {

    var currentVal = select2element.val();
    var newVal = [newKey];

    if (currentVal !== null) {
        var uniteVal = currentVal.concat(newVal.filter(function (item) {
            return currentVal.indexOf(item) < 0;
        }));
    }
    else {
        uniteVal = newVal;
    }

    //find option
    var existOption = select2element.find("[value='" + newKey + "']");

    if (existOption.length == 0) {
        var newOption = $("<option></option>").val(newKey).text(newValue);
        select2element.append(newOption);
    }

    select2element.val(uniteVal);
}

function updateAttributesTable(select2, tableId) {
    var table = $('#' + tableId);
    var selectedAttributes = select2.val();
    var attributesUrl = select2.attr('data-attributes-by-id-url');

    //перебратьвсе тр - удалить лишние, те, что есть не трогаем
    if (selectedAttributes != null) {
        table.find('tr:not(:first)').each(function () {

            if (selectedAttributes.indexOf($(this).attr('id')) == -1) {
                $(this).remove();
            }
        });
    }
    else {
        table.find('tr:not(:first)').remove();
    }


    //table.find( 'tr:not(:first)' ).remove();
    //get group attributes value
    $.ajax({
        method: "POST",
        url: attributesUrl,
        data: {attributesIds: selectedAttributes}
    })
        .done(function (attributes) {
            var attributesObjsArray = JSON.parse(attributes);

            for (var i = 0; i < attributesObjsArray.length; i++) {

                if (table.find('tr#' + attributesObjsArray[i].id).length == 0) {
                    addTrToTable(table, attributesObjsArray[i]);
                }
            }
        });


    // alert(select2.val());
}

function addTrToTable(table, attributeObj) {

    var TYPE_TEXT_FIELD = '1';
    var TYPE_DROP_DOWN_FIELD = '2';
    var TYPE_MULTI_SELECT_FIELD = '3';

    var tableId = table.attr('id');
    var select2id = '';

    var attributeId = attributeObj.id;
    var attributeLabel = attributeObj.label;
    var productId = table.attr('data-model-id');

    //code below use for related form
    var className = table.attr('data-class-name');
    var relatedIndex = table.parents('.content-append').attr('data-index');

    if (relatedIndex != undefined){
        className += '['+relatedIndex+']';
    }


    switch (attributeObj.attribute_type) {
        case TYPE_TEXT_FIELD:
            //get value
            var value = '';
            $.ajax({
                method: "POST",
                url: table.attr('data-get-value-url'),
                data: {
                    attributeId: attributeId,
                    productId: productId,
                    attributeType: TYPE_TEXT_FIELD,
                    attributeLabel:  attributeLabel,
                    tableId: tableId,
                    className: className
                }
            })
                .done(function (data) {
                    var response = JSON.parse(data);
                    parseResponse(response);
                });

            break;
        case TYPE_DROP_DOWN_FIELD:
            select2id = Math.random().toString(36).substring(7);

            //get value
            $.ajax({
                method: "POST",
                url: table.attr('data-get-value-url'),
                data: {
                    attributeId: attributeId,
                    productId: productId,
                    attributeType: TYPE_DROP_DOWN_FIELD,
                    attributeLabel:  attributeLabel,
                    tableId: tableId,
                    select2id: select2id,
                    className: className
                }
            })
                .done(function (data) {
                    var response = JSON.parse(data);
                    parseResponse(response);

                     $('#' + select2id).select2();

                });

            break;
        case TYPE_MULTI_SELECT_FIELD:
            select2id = Math.random().toString(36).substring(7);

            //get value
            $.ajax({
                method: "POST",
                url: table.attr('data-get-value-url'),
                data: {
                    attributeId: attributeId,
                    productId: productId,
                    attributeType: TYPE_MULTI_SELECT_FIELD,
                    attributeLabel:  attributeLabel,
                    tableId: tableId,
                    select2id: select2id,
                    className: className
                }
            })
                .done(function (data) {
                    var response = JSON.parse(data);
                    parseResponse(response);

                    $('#' + select2id).select2();

                });
            break;
    }

}

function getAttributeValue(url, attributeId, productId) {
    $.ajax({
        method: "POST",
        url: url,
        data: {attributeId: attributeId, productId: productId}
    })
        .done(function (values) {
            console.log(values);
            return values;

        });
}

//use for related forms
function updateRelatedIndex(className, selectAttributeId, selectGroupId) {

    var selectAttributeElement = $('#'+selectAttributeId);
    var selectGroupElement = $('#'+selectGroupId);
    var relatedIndex = selectAttributeElement.parents('.content-append').attr('data-index');

    if (relatedIndex != undefined){
        className = className+'['+ relatedIndex + ']';

        var selectAttributeRelatedName = className+'[attributesSelected][]';
        selectAttributeElement.attr('name',selectAttributeRelatedName);

        var selectGroupRelatedName = className+'[attributeGroupsSelected][]';
        selectGroupElement.attr('name',selectGroupRelatedName);
    }
}

// $(document).ready(function () {
//     if ($('.choose-product-attribute').length > 0){
//
//         alert(1);
//         $('.choose-product-attribute').each(function () {
//             var tableId = $(this).attr('data-table-id');
//             var groupId = $(this).attr('data-group-id');
//             var attributeId = $(this).attr('id');
//             var className = $(this).attr('data-class-name');
//
//             updateAttributesTable($(this),tableId);
//             updateRelatedIndex(className, attributeId, groupId);
//         });
//
//     }
//
//
// });
