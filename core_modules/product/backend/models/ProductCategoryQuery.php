<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 01.03.2017
 * Time: 13:07
 */
namespace backend\modules\product\models;

use backend\modules\product\components\NestedSetsQueryBehavior;
use yii\db\ActiveQuery;

class ProductCategoryQuery extends ActiveQuery
{
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
}
