<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;

/**
 * This is the model class for table "{{%product_category_to_attribute}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $attribute_id
 *
 * @property ProductAttribute $attribute0
 * @property ProductCategory $category
 */
class ProductCategoryToAttribute extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_category_to_attribute}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'attribute_id'], 'required'],
            [['category_id', 'attribute_id'], 'integer'],
            [
                ['category_id'],
                'exist',
                'targetClass' => \common\models\ProductCategory::className(),
                'targetAttribute' => 'id'
            ],
            [
                ['attribute_id'],
                'exist',
                'targetClass' => \common\models\ProductAttribute::className(),
                'targetAttribute' => 'id'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-category-to-attribute', 'ID'),
            'category_id' => Yii::t('back/product-category-to-attribute', 'Category'),
            'attribute_id' => Yii::t('back/product-category-to-attribute', 'Attribute'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasOne(ProductAttribute::className(), ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-category-to-attribute', 'Product Category To Attribute');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'category_id',
                    'attribute_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'category_id',
                    'attribute_id',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductCategoryToAttributeSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'category_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ProductCategory::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'attribute_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ProductAttribute::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ];

        return $config;
    }
}
