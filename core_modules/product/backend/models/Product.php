<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use backend\components\TranslateableTrait;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\product\components\ManyToManyProductBehavior;
use backend\modules\product\helpers\ImgHelper;
use backend\modules\product\helpers\Select2Helper;
use backend\modules\product\widgets\attributesWidget\AttributesWidget;
use backend\modules\product\widgets\oneToManyWidget\OneToManyWidget;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\models\EntityToFile;
use common\models\Product as CommonProduct;
use common\models\ProductToAttributeGroup;
use kartik\tree\TreeViewInput;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $id
 * @property integer $is_packing
 * @property integer $parent_product_id
 * @property string $packing_link_text
 * @property string $sku
 * @property string $label
 * @property string $alias
 * @property integer $brand_id
 * @property integer $manufacturer_id
 * @property integer $availability_id
 * @property string $short_desc
 * @property string $content
 * @property string $price
 * @property string $old_price
 * @property integer $price_type
 * @property double $rating_total
 * @property integer $rating_voice_count
 * @property integer $show_as_separate
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductAvailability $availability
 * @property ProductBrand $brand
 * @property ProductManufacturer $manufacturer
 * @property Product $parentProduct
 * @property Product[] $products
 * @property ProductTranslation[] $translations
 */
class Product extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;

    /**
     * main product main image
     * @var
     */
    public $titleImage;
    public $categoryIds;
    public $deliveryIds;
    public $deliveryIdsValue;
    public $storehouseIds;
    public $storehouseIdsValue;
    public $similarIds;
    public $similarIdsValue;
    public $videos;
    public $attributeGroupsSelected;
    public $attributeTable;



    /**
     * Temporary sign which used for saving images before model save
     * @var
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'is_packing',
                    'parent_product_id',
                    'brand_id',
                    'manufacturer_id',
                    'availability_id',
                    'price_type',
                    'rating_voice_count',
                    'show_as_separate',
                    'published',
                    'position'
                ],
                'integer'
            ],
            [['label', 'alias'], 'required'],
            [['content'], 'string'],
            [['price', 'old_price', 'rating_total'], 'number'],
            [['packing_link_text', 'sku', 'label', 'alias', 'short_desc'], 'string', 'max' => 255],
            [['is_packing'], 'default', 'value' => 0],
            [
                ['parent_product_id'],
                'exist',
                'targetClass' => CommonProduct::className(),
                'targetAttribute' => 'id'
            ],
            [
                ['brand_id'],
                'exist',
                'targetClass' => \common\models\ProductBrand::className(),
                'targetAttribute' => 'id'
            ],
            [
                ['manufacturer_id'],
                'exist',
                'targetClass' => \common\models\ProductManufacturer::className(),
                'targetAttribute' => 'id'
            ],
            [
                ['availability_id'],
                'exist',
                'targetClass' => \common\models\ProductAvailability::className(),
                'targetAttribute' => 'id'
            ],
            [['show_as_separate'], 'default', 'value' => 1],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            ['sku', 'validateUnique'],
            [['categoryIds', 'videos', 'deliveryIds', 'storehouseIds', 'similarIds', 'sign','attributeGroupsSelected', 'attributeTable'], 'safe']
        ];
    }

    public function validateUnique($attribute, $params)
    {

        $count = Product::find()
            ->where([$attribute => $this->{$attribute}]);

        if ($this->id != null) {
            $count->andWhere(['<>', 'id', $this->id]);
        }

        if ($count->count() > 0) {
            $this->addError($attribute, Yii::t('back/product', 'Field not unique'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product', 'ID'),
            'is_packing' => Yii::t('back/product', 'Is packing'),
            'parent_product_id' => Yii::t('back/product', 'Parent product'),
            'packing_link_text' => Yii::t('back/product', 'Packing link text'),
            'sku' => Yii::t('back/product', 'SKU'),
            'label' => Yii::t('back/product', 'Label'),
            'alias' => Yii::t('back/product', 'Alias'),
            'brand_id' => Yii::t('back/product', 'Brand'),
            'manufacturer_id' => Yii::t('back/product', 'Manufacturer'),
            'availability_id' => Yii::t('back/product', 'Availability'),
            'short_desc' => Yii::t('back/product', 'Short description'),
            'content' => Yii::t('back/product', 'Content'),
            'price' => Yii::t('back/product', 'Price'),
            'old_price' => Yii::t('back/product', 'Old Price'),
            'price_type' => Yii::t('back/product', 'Price type'),
            'rating_total' => Yii::t('back/product', 'Rating total'),
            'rating_voice_count' => Yii::t('back/product', 'Rating voice count'),
            'show_as_separate' => Yii::t('back/product', 'Show as separate'),
            'published' => Yii::t('back/product', 'Published'),
            'position' => Yii::t('back/product', 'Position'),
            'created_at' => Yii::t('back/product', 'Created At'),
            'updated_at' => Yii::t('back/product', 'Updated At'),
            'titleImage' => Yii::t('back/product', 'Photos'),
            'categoryIds' => Yii::t('back/product', 'Categories'),
            'videos' => Yii::t('back/product', 'Video'),
            'deliveryIds' => Yii::t('back/product', 'Delivery types'),
            'storehouseIds' => Yii::t('back/product', 'Storehouse'),
            'attributes' => Yii::t('back/product', 'Attributes'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'packing_link_text',
            'label',
            'short_desc',
            'content',
        ];
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'manyToManyProduct' => [
                'class' => ManyToManyProductBehavior::className(),
                'currentObj' => $this,
                'currentRelatedColumn' => 'product_id',
                'manyToManyConfig' => [
                    'delivery' => [
                        'attribute' => 'deliveryIds',
                        'valueAttribute' => 'deliveryIdsValue',
                        'relatedColumn' => 'delivery_id',
                        'linkTableName' => ProductToDelivery::tableName(),
                        'relatedObjTableName' => ProductDelivery::tableName(),
                        'isStringReturn' => false
                    ],
                    'storehouse' => [
                        'attribute' => 'storehouseIds',
                        'valueAttribute' => 'storehouseIdsValue',
                        'relatedColumn' => 'storehouse_id',
                        'linkTableName' => ProductToStorehouse::tableName(),
                        'relatedObjTableName' => ProductStorehouse::tableName(),
                        'isStringReturn' => false
                    ],
                    'category' => [
                        'attribute' => 'categoryIds',
                        'valueAttribute' => 'categoryIds',
                        'relatedColumn' => 'category_id',
                        'linkTableName' => ProductToCategory::tableName(),
                        'relatedObjTableName' => ProductCategory::tableName(),
                        'isStringReturn' => true
                    ],
                    'similar' => [
                        'attribute' => 'similarIds',
                        'valueAttribute' => 'similarIdsValue',
                        'relatedColumn' => 'similar_id',
                        'linkTableName' => ProductToSimilar::tableName(),
                        'relatedObjTableName' => Product::tableName(),
                        'isStringReturn' => false
                    ],
                ]

            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainImage()
    {
        $class = $this::formName();
        if ($this->is_packing) {
            $class = StringHelper::basename(ProductPacking::className());
        }
        return $this->hasOne(\common\models\EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => $class])
            ->from(['t2' => \common\models\EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvailability()
    {
        return $this->hasOne(ProductAvailability::className(), ['id' => 'availability_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(ProductBrand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(ProductManufacturer::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'parent_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacking()
    {
        return $this->hasMany(ProductPacking::className(), ['parent_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeGroups()
    {
        return $this->hasMany(
            ProductAttributeGroup::className(),
            ['id' => 'attribute_group_id']
        )->viaTable(
            ProductToAttributeGroup::tableName(),
            ['product_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributesObj()
    {
        return $this->hasMany(
            ProductAttribute::className(),
            ['id' => 'attribute_id']
        )->viaTable(
            ProductEav::tableName(),
            ['product_id' => 'id']
        );
    }


    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product', 'Product');
    }

    /**
     * For RelatedFormWidget
     * @return array
     */
    public function getRelatedFormConfig()
    {
        $module = \Yii::$app->controller->module;

        if (($module->params['usePacking'])
            ||
            ((!$module->params['usePacking']) && ($module->params['deleteExistingPackingWhenPackingIsDisabled']))
        ) {
            return [
                'packing' => [
                    'relation' => 'packing',
                ],
            ];
        } else {
            return [];
        }
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'sku',
                    'is_packing',
                    'parent_product_id',
                    'packing_link_text:url',
                    'alias',
                    [
                        'attribute' => 'titleImage',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ImgHelper::getEntityImage($data);
                        }
                    ],
                    // 'brand_id',
                    // 'manufacturer_id',
                    // 'availability_id',
                    // 'short_desc',
                    // 'content:ntext',
                    // 'price',
                    // 'old_price',
                    // 'price_type',
                    // 'rating_total',
                    // 'rating_voice_count',
                    // 'show_as_separate:boolean',
                    'published:boolean',
                    'position',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                if ($model->is_packing) {
                                    return '';
                                }
                                $url = Yii::$app->getUrlManager()->createUrl([
                                    '/product/product/update',
                                    'id' => $model->id
                                ]);
                                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url,
                                    ['title' => Yii::t('yii', 'Update'), 'data-pjax' => '0']);
                            }
                        ],

                    ],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'is_packing:boolean',
                    'parent_product_id',
                    'packing_link_text:url',
                    'sku',
                    'label',
                    'alias',
                    [
                        'attribute' => 'titleImage',
                        'format' => 'raw',
                        'value' => ImgHelper::getEntityImage($this)
                    ],
                    'brand_id',
                    'manufacturer_id',
                    'availability_id',
                    'short_desc',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'price',
                    'old_price',
                    'price_type',
                    'rating_total',
                    'rating_voice_count',
                    'show_as_separate:boolean',
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductSearch();
    }

    /**
     * @return string
     */
    public static function getDeliveryUrl()
    {
        return '/product/product-delivery/get-select-items';
    }

    /**
     * @return string
     */
    public static function getStorehouseUrl()
    {
        return '/product/product-storehouse/get-select-items';
    }

    /**
     * @return string
     */
    public static function getSimilarUrl()
    {
        return '/product/product/get-select-items';
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $module = \Yii::$app->controller->module;
        //$module->params['usePacking']

        $config = [
            'form-set' => [
                Yii::t('back/product', 'Main') => [
                    'sku' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'fieldOptions' => [
                            'enableAjaxValidation' => true,
                        ],
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => 's_name form-control'
                        ],
                    ],
                    'alias' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => 's_alias form-control'
                        ],
                    ],
                    'titleImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'titleImage',
                            'saveAttribute' => CommonProduct::SAVE_ATTRIBUTE_IMAGES,
                            'aspectRatio' => 420 / 210,
                            'multiple' => true,
                        ])
                    ],
                    'videos' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => OneToManyWidget::className(),
                        'options' => [
                            'data' => $this->getVideos()
                        ]
                    ],
                    'attributes' => [
                        'type' => $module->params['useEav'] ? ActiveFormBuilder::INPUT_WIDGET : ActiveFormBuilder::INPUT_RAW,
                        'label' => $module->params['useEav'] ? Yii::t('back/product', 'Attributes') : false,
                        'widgetClass' => AttributesWidget::className(),
                    ],
                    'categoryIds' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        //'value' => '4',
                        'widgetClass' => TreeViewInput::className(),
                        'options' => [
                            'query' => ProductCategory::find()->addOrderBy('root, left'),
                        ]

                    ],
                    'brand_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\ProductBrand::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'manufacturer_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\ProductManufacturer::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'availability_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\ProductAvailability::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'deliveryIds' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('deliveryIds'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this,
                            'deliveryIds',
                            static::getDeliveryUrl(),
                            $this->deliveryIdsValue,
                            [
                                'multiple' => true,
                            ]
                        )
                    ],
                    'storehouseIds' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('storehouseIds'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this,
                            'storehouseIds',
                            static::getStorehouseUrl(),
                            $this->storehouseIdsValue,
                            [
                                'multiple' => true,
                            ]
                        )
                    ],
                    'similarIds' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('similarIds'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this,
                            'similarIds',
                            static::getSimilarUrl(),
                            $this->similarIdsValue,
                            [
                                'multiple' => true,
                            ]
                        )
                    ],
                    'short_desc' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'content' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => \backend\components\ImperaviContent::className(),
                        'options' => [
                            'model' => $this,
                            'attribute' => 'content',
                        ]
                    ],
                    'price' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'old_price' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'price_type' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => \common\models\ProductPriceType::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'rating_total' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'rating_voice_count' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'packing_link_text' => [
                        'type' => $module->params['usePacking'] ? ActiveFormBuilder::INPUT_TEXT : ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => $module->params['usePacking'] ? Yii::t('back/product', 'Packing link text') : false,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'position' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
            ],
        ];

        if (($module->params['usePacking']) && (!$this->is_packing)) {
            $config['form-set'][Yii::t('back/product', 'Packing')] = [
                $this->getRelatedFormConfig()['packing']
            ];
        }

        return $config;
    }

    public function getVideos()
    {
        return ArrayHelper::map(
            ProductVideo::find()
                ->where(['product_id' => $this->id])
                ->orderBy(['position' => SORT_ASC])
                ->all(),
            'id',
            'video_link'
        );
    }

    /**
     *
     */
    protected function saveVideos()
    {
        ProductVideo::deleteAll([
            'product_id' => $this->id
        ]);
        //save video
        $position = 0;

        if ($this->videos != null) {
            foreach ($this->videos as $video) {
                if ($video != '') {
                    $newPhone = new ProductVideo();
                    $newPhone->video_link = $video;
                    $newPhone->product_id = $this->id;
                    $newPhone->position = $position;
                    $newPhone->save();

                    $position++;
                }
            }
        }
    }

    /**
     *
     */
    protected function saveAttributes()
    {
        //delete old
        ProductToAttributeGroup::deleteAll(['product_id' => $this->id]);
        ProductEav::deleteAll(['product_id' => $this->id]);

        //save attributes
        if (isset($this->attributeGroupsSelected)) {
            $attributesGroup = $this->attributeGroupsSelected;

            foreach ($attributesGroup as $groupId) {
                $productToGroup = new ProductToAttributeGroup();
                $productToGroup->product_id = $this->id;
                $productToGroup->attribute_group_id = $groupId;
                $productToGroup->save();
            }
        }

        if (isset($this->attributeTable)) {
            $attributesValues = $this->attributeTable;

            foreach ($attributesValues as $attributeId => $attributeValue) {
                if (is_array($attributeValue)) {
                    foreach ($attributeValue as $multipleOption) {
                        $productEav = new ProductEav();
                        $productEav->product_id = $this->id;
                        $productEav->attribute_id = $attributeId;
                        $productEav->value = $multipleOption;
                        $productEav->save();
                    }
                } else {
                    $productEav = new ProductEav();
                    $productEav->product_id = $this->id;
                    $productEav->attribute_id = $attributeId;
                    $productEav->value = $attributeValue;
                    $productEav->save();
                }
            }
        }
    }


    public function beforeSave($insert)
    {
        if ($this->sku == '') {
            $this->sku = null;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $module = \Yii::$app->controller->module;

        if ($module->params['useEav']) {
            $this->saveAttributes();
        }

        $this->saveVideos();

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
