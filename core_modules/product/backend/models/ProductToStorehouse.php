<?php

namespace backend\modules\product\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%product_to_storehouse}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $storehouse_id
 *
 * @property Product $product
 * @property ProductStorehouse $storehouse
 */
class ProductToStorehouse extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_to_storehouse}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'storehouse_id'], 'required'],
            [['product_id', 'storehouse_id'], 'integer'],
            [['product_id'], 'exist', 'targetClass' => \common\models\Product::className(), 'targetAttribute' => 'id'],
            [['storehouse_id'], 'exist', 'targetClass' => \common\models\ProductStorehouse::className(), 'targetAttribute' => 'id'],
                ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-to-storehouse', 'ID'),
            'product_id' => Yii::t('back/product-to-storehouse', 'Product'),
            'storehouse_id' => Yii::t('back/product-to-storehouse', 'Storehouse'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorehouse()
    {
        return $this->hasOne(ProductStorehouse::className(), ['id' => 'storehouse_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/product-to-storehouse', 'Product To Storehouse');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'product_id',
                    'storehouse_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'product_id',
                    'storehouse_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ProductToStorehouseSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'product_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Product::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'storehouse_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ProductStorehouse::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ];

        return $config;
    }
}
