<?php

namespace backend\modules\product\models;

use backend\components\TranslateableTrait;
use backend\modules\product\components\ManyToManyProductBehavior;
use backend\modules\product\helpers\Select2Helper;
use creocoder\translateable\TranslateableBehavior;
use kartik\widgets\Select2;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use yii\behaviors\TimestampBehavior;
use yii\web\JsExpression;

/**
 * This is the model class for table "{{%product_attribute_group}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductAttributeGroupTranslation[] $translations
 */
class ProductAttributeGroup extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;

    public $attributeIds;
    public $attributeIdsValue;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_attribute_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['published'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['attributeIds'], 'safe']
                ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-attribute-group', 'ID'),
            'label' => Yii::t('back/product-attribute-group', 'Label'),
            'published' => Yii::t('back/product-attribute-group', 'Published'),
            'created_at' => Yii::t('back/product-attribute-group', 'Created At'),
            'updated_at' => Yii::t('back/product-attribute-group', 'Updated At'),
            'attributeIds' => Yii::t('back/product-attribute', 'Attributes'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'manyToManyProduct' => [
                'class' => ManyToManyProductBehavior::className(),
                'currentObj' => $this,
                'currentRelatedColumn' => 'attribute_group_id',
                'manyToManyConfig' => [
                    'attribute_group' =>[
                        'attribute' => 'attributeIds',
                        'valueAttribute' => 'attributeIdsValue',
                        'relatedColumn' => 'attribute_id',
                        'linkTableName' => ProductAttributeToAttributeGroup::tableName(),
                        'relatedObjTableName' => ProductAttribute::tableName(),
                        'isStringReturn' => false
                    ],

                ]

            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductAttributeGroupTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/product-attribute-group', 'Product Attribute Group');
    }

    /**
     * @return string
     */
    public static function getAttributeUrl()
    {
        return '/product/product-attribute/get-select-items';
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'published:boolean',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    'published:boolean',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ProductAttributeGroupSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'attributeIds' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'label' => $this->getAttributeLabel('attributeIds'),
                'value' => Select2Helper::getAjaxSelect2Widget(
                    $this,
                    'attributeIds',
                    static::getAttributeUrl(),
                    $this->attributeIdsValue,
                    [
                        'multiple' => true,
                    ]
                )
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }
}
