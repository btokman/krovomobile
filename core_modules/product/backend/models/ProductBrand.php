<?php

namespace backend\modules\product\models;

use backend\components\ImperaviContent;
use backend\components\TranslateableTrait;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\product\helpers\ImgHelper;
use common\models\EntityToFile;
use common\models\ProductBrand as CommonProductBrand;
use creocoder\translateable\TranslateableBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%product_brand}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property string $short_desc
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductBrandTranslation[] $translations
 */
class ProductBrand extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;


    /**
     * main brand image
     * @var
     */
    public $titleImage;

    /**
     * Temporary sign which used for saving images before model save
     * @var
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_brand}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'alias'], 'required'],
            [['content'], 'string'],
            [['sign'], 'safe'],
            [['published', 'position'], 'integer'],
            [['label', 'alias', 'short_desc'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-brand', 'ID'),
            'label' => Yii::t('back/product-brand', 'Label'),
            'alias' => Yii::t('back/product-brand', 'Alias'),
            'short_desc' => Yii::t('back/product-brand', 'Short Description'),
            'content' => Yii::t('back/product-brand', 'Content'),
            'published' => Yii::t('back/product-brand', 'Published'),
            'position' => Yii::t('back/product-brand', 'Position'),
            'created_at' => Yii::t('back/product-brand', 'Created At'),
            'updated_at' => Yii::t('back/product-brand', 'Updated At'),
            'titleImage' => Yii::t('back/product-brand', 'Title image'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'short_desc',
            'content',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainImage()
    {
        return $this->hasOne(\common\models\EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName()])
            ->from(['t2' => \common\models\EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductBrandTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-brand', 'Product Brand');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'alias',
                    'short_desc',
                    [
                        'attribute' => 'titleImage',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ImgHelper::getEntityImage($data);
                        }
                    ],
                    // 'content:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'alias',
                    'short_desc',
                    [
                        'attribute' => 'titleImage',
                        'format' => 'raw',
                        'value' => ImgHelper::getEntityImage($this)
                    ],
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductBrandSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_name form-control'
                ],
            ],
            'alias' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_alias form-control'
                ],
            ],
            'short_desc' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'titleImage' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'titleImage',
                    'saveAttribute' => CommonProductBrand::SAVE_ATTRIBUTE_IMAGES,
                    'aspectRatio' => 420 / 210,
                    'multiple' => false,
                ])
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
