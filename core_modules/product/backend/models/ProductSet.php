<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use backend\modules\product\helpers\Select2Helper;
use common\components\model\ActiveRecord;
use kartik\widgets\Select2;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/**
 * This is the model class for table "{{%product_set}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $product_set_id
 * @property double $discount
 * @property string $price
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Product $product
 * @property Product $productSet
 */
class ProductSet extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_set}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'product_set_id'], 'required'],
            [['product_id', 'product_set_id'], 'integer'],
            [['discount', 'price'], 'number'],
            [['product_id'], 'exist', 'targetClass' => \common\models\Product::className(), 'targetAttribute' => 'id'],
            [
                ['product_set_id'],
                'exist',
                'targetClass' => \common\models\Product::className(),
                'targetAttribute' => 'id'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-set', 'ID'),
            'product_id' => Yii::t('back/product-set', 'Main product in set'),
            'product_set_id' => Yii::t('back/product-set', 'Second product in set'),
            'discount' => Yii::t('back/product-set', 'set discount'),
            'price' => Yii::t('back/product-set', 'set price'),
            'created_at' => Yii::t('back/product-set', 'Created At'),
            'updated_at' => Yii::t('back/product-set', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSet()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_set_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-set', 'Product Set');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'product_id',
                    'product_set_id',
                    'discount',
                    'price',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'product_id',
                    'product_set_id',
                    'discount',
                    'price',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductSetSearch();
    }

    /**
     * @return string
     */
    public static function getProductUrl()
    {
        return '/product/product/get-select-items';
    }

    public function getSelectedProductData()
    {
        $product = Product::find()
            ->select(['id', 'label'])
            ->where(['id' => $this->product_id])
            ->asArray()
            ->all();

        return ArrayHelper::map($product, 'id', 'label');
    }

    public function getSelectedProductSetData()
    {
        $productSet = Product::find()
            ->select(['id', 'label'])
            ->where(['id' => $this->product_set_id])
            ->asArray()
            ->all();

        return ArrayHelper::map($productSet, 'id', 'label');
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'product_id' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'label' => $this->getAttributeLabel('product_id'),
                'value' => Select2Helper::getAjaxSelect2Widget(
                    $this,
                    'product_id',
                    static::getProductUrl(),
                    $this->getSelectedProductData(),
                    [
                        'multiple' => false,
                    ]
                )
            ],
            'product_set_id' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'label' => $this->getAttributeLabel('product_set_id'),
                'value' => Select2Helper::getAjaxSelect2Widget(
                    $this,
                    'product_set_id',
                    static::getProductUrl(),
                    $this->getSelectedProductSetData(),
                    [
                        'multiple' => false,
                    ]
                )
            ],
            'discount' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'price' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
        ];

        return $config;
    }
}
