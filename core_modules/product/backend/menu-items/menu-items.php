<?php

return [
    'label' => Yii::t('back/product', 'product'),
    'items' => [
        [
            'label' => Yii::t('back/product', 'product'),
            'url' => ['/product/product/index'],
        ],
        [
            'label' => Yii::t('back/product', 'brand'),
            'url' => ['/product/product-brand/index'],
        ],
        [
            'label' => Yii::t('back/product', 'manufacturer'),
            'url' => ['/product/product-manufacturer/index'],
        ],
        [
            'label' => Yii::t('back/product', 'category'),
            'url' => ['/product/product-category/tree'],
        ],
        [
            'label' => Yii::t('back/product', 'product-set'),
            'url' => ['/product/product-set/index'],
        ],
        [
            'label' => Yii::t('back/product', 'delivery'),
            'url' => ['/product/product-delivery/index'],
        ],
        [
            'label' => Yii::t('back/product', 'storehouse'),
            'url' => ['/product/product-storehouse/index'],
        ],
        [
            'label' => Yii::t('back/product', 'price type'),
            'url' => ['/product/product-price-type/index'],
        ],
        [
            'label' => Yii::t('back/product', 'availability'),
            'url' => ['/product/product-availability/index'],
        ],
        [
            'label' => Yii::t('back/product', 'attribute'),
            'items' => [
                [
                    'label' => Yii::t('back/product', 'attribute'),
                    'url' => ['/product/product-attribute/index'],
                ],
                [
                    'label' => Yii::t('back/product', 'attribute group'),
                    'url' => ['/product/product-attribute-group/index'],
                ],
            ]
        ],
    ]
];
