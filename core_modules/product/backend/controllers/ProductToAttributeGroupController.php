<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductToAttributeGroup;

/**
 * ProductToAttributeGroupController implements the CRUD actions for ProductToAttributeGroup model.
 */
class ProductToAttributeGroupController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductToAttributeGroup::className();
    }
}
