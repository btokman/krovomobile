<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductStorehousePhone;

/**
 * ProductStorehousePhoneController implements the CRUD actions for ProductStorehousePhone model.
 */
class ProductStorehousePhoneController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductStorehousePhone::className();
    }
}
