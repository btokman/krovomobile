<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductBrand;

/**
 * ProductBrandController implements the CRUD actions for ProductBrand model.
 */
class ProductBrandController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductBrand::className();
    }
}
