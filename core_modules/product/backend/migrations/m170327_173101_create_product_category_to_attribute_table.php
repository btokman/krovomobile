<?php

use console\components\Migration;

/**
 * Class m170327_173101_create_product_category_to_attribute_table migration
 */
class m170327_173101_create_product_category_to_attribute_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_category_to_attribute}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'category_id' => $this->integer()->notNull()->comment('Category'),
                'attribute_id' => $this->integer()->notNull()->comment('Attribute'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-category_to_attribute-category_id-product_category-id',
            $this->tableName,
            'category_id',
            '{{%product_category}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-category_to_attribute-attribute_id-product_attribute-id',
            $this->tableName,
            'attribute_id',
            '{{%product_attribute}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-category_to_attribute-category_id-product_category-id', $this->tableName);
        $this->dropForeignKey('fk-category_to_attribute-attribute_id-product_attribute-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
