<?php

use console\components\Migration;

/**
 * Class m170319_142209_create_product_to_attribute_group_table migration
 */
class m170319_142209_create_product_to_attribute_group_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_to_attribute_group}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'product_id' => $this->integer()->notNull()->comment('Product'),
                'attribute_group_id' => $this->integer()->notNull()->comment('Attribute group'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-product_to_attribute_group-product_id-product-id',
            $this->tableName,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-attribute_to_group-group_id-group-id',
            $this->tableName,
            'attribute_group_id',
            '{{%product_attribute_group}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_to_attribute_group-product_id-product-id', $this->tableName);
        $this->dropForeignKey('fk-attribute_to_group-group_id-group-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
