<?php

use console\components\Migration;

/**
 * Class m170309_171655_create_product_storehouse_table migration
 */
class m170309_171655_create_product_storehouse_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_storehouse}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'address' => $this->string()->null()->comment('Address'),
                'work_time' => $this->string()->null()->comment('Work time'),
                'lat' => $this->float()->null()->comment('Latitude'),
                'lng' => $this->float()->null()->comment('Longitude'),
                'short_desc' => $this->string()->null()->comment('Short Description'),
                'content' => $this->text()->null()->comment('Content'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
