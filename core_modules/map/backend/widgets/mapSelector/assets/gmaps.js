var latitude = parseFloat($('#mapentity-latitude').val());
var longitude = parseFloat($('#mapentity-longitude').val());
var zoom = parseFloat($('#mapentity-zoom').val());

var baseCoordinates = {lat: latitude, lng: longitude};

var mapElement = $('.map-class');
var mapID = mapElement.attr('id');

var map = new google.maps.Map(document.getElementById(mapID), {
    zoom: zoom,
    center: baseCoordinates,
    scrollwheel: mapElement.data('scrollable'),
    styles: [{
        stylers: [{
            saturation: -100
        }]
    }]
});


var markers =  [
    new google.maps.Marker({
        position: baseCoordinates,
        map: map,
        icon: mapElement.data('marker')
    })
];

//fix display on tab
$(document).on('click', "ul.nav-tabs li", function() {
    if($(map).length) {
        google.maps.event.trigger(map, 'resize');
        map.setCenter(baseCoordinates);
        map.setZoom(zoom);
    }
});

//try to get coordinates with stackoverflow
google.maps.event.addListener(map, "click", function(event) {
    var lat = event.latLng.lat();
    var lng = event.latLng.lng();
    $('#mapentity-latitude').val(lat);
    $('#mapentity-longitude').val(lng);
    clearMarkers();

    mark = new google.maps.Marker({
        position: {lat: lat, lng: lng},
        map: map,
        icon: mapElement.data('marker')
    });
    markers.push(mark);
});

google.maps.event.addListener(map, 'zoom_changed', function(e) {
    var zoom = this.getZoom();
    $('#mapentity-zoom').val(zoom).trigger('change');
});

// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}
// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

// Auto complete input
var input = document.getElementById('auto-complete');
if (input) {
    var autoComplete = new google.maps.places.Autocomplete(document.getElementById('auto-complete'));
    google.maps.event.addListener(autoComplete, 'place_changed', function () {

        var place = autoComplete.getPlace();
        if (!place.geometry) {
            return
        }
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
        }
    });
}

