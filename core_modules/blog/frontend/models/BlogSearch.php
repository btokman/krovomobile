<?php

namespace frontend\modules\blog\models;

use yii\data\ActiveDataProvider;

/**
 * BlogSearch represents the model behind the search form about `Blog`.
 */
class BlogSearch extends Blog
{
    /**
     * @var int fixed blog post ID.
     */
    public $fixedPostId;

    /**
     * @var int index page size.
     */
    public $pageSize = 10;

    /**
     * @var \yii\db\ActiveQuery|null
     */
    private $_query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'published', 'author_id'], 'integer'],
            [['label', 'content', 'publish_at', 'alias', 'description'], 'safe'],
        ];
    }

    /**
     * Query getter.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuery()
    {
        if ($this->_query === null) {
            $this->_query = self::find()
                ->joinWith(['image', 'blogCategories', 'blogAuthor'])
                ->andWhere(['blog.published' => 1])
                ->andWhere(['<=', 'publish_at', date('Y-m-d H:i')]);
        }

        return $this->_query;
    }

    /**
     * Search for blog posts.
     *
     * @param array $params query params.
     * @return array [dataProvider, fixed]
     */
    public function searchAll($params)
    {
        $output = [];

        /**
         * @var string|null $category category alias.
         */
        extract($params);
        $query = $this->getQuery();

        if (isset($category) && !empty($category)) {
            // Applying category condition.
            $query->andWhere(['blog_category.alias' => $category]);
        }

        $fixedQuery = clone $query;

        if ($this->fixedPostId) {
            $fixed = $fixedQuery->andWhere(['blog.id' => $this->fixedPostId])->one();
            // Decrease page size by one, to fit page size limit with fixed post.
            !$fixed ?: --$this->pageSize;
            $output = compact('fixed');
            // Forbid selecting fixed post in general query.
            $query->andWhere(['!=', 'blog.id', $this->fixedPostId]);
        }

        $output['dataProvider'] = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                // next two params must be equal to remove pageSizeParam from URL
                'pageSize' => $this->pageSize,
                'pageSizeParam' => false,
            ],
            'sort' => [
                'defaultOrder' => [
                    'publish_at' => SORT_DESC,
                ]
            ],
        ]);

        return $output;
    }
}
