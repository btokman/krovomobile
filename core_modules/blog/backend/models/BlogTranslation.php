<?php

namespace backend\modules\blog\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%blog_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $content
 * @property string $description
 */
class BlogTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/blog', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/blog', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/blog', 'Label') . ' [' . $this->language . ']',
            'content' => Yii::t('back/blog', 'Content') . ' [' . $this->language . ']',
            'description' => Yii::t('back/blog', 'Description') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'description'], 'string'],
            [['label'], 'string', 'max' => 255],
        ];
    }
}
