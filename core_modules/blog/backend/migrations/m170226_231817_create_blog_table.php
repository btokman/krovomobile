<?php

use console\components\Migration;

/**
 * Class m170226_231817_create_blog_table migration
 */
class m170226_231817_create_blog_table extends Migration
{
    /**
     * @var string migration table name
     */
    public $tableName = '{{%blog}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'author_id' => $this->integer()->null()->comment('Author ID'),
                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->notNull()->unique()->comment('Alias'),
                'description' => $this->text()->null()->comment('Description'),
                'content' => $this->text()->null()->comment('Content'),
                'published' => $this->smallInteger(1)->notNull()->defaultValue(1)->comment('Published'),
                'publish_at' => $this->dateTime()->notNull()->comment('Publish at'),
                'created_at' => $this->integer()->notNull()->comment('Created at'),
                'updated_at' => $this->integer()->notNull()->comment('Updated at'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
