<?php

namespace frontend\modules\faq\widgets\emailAdmin;

use common\models\FaqRequest;
use yii\base\Widget;

class EmailAdminWidget extends Widget
{
    /** @var FaqRequest */
    public $model;

    public function run()
    {
        return $this->render('default', ['model' => $this->model]);
    }
}
