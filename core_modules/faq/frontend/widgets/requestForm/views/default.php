<?php
/**
 * @var \yii\web\View $this
 * @var FaqRequestForm $model
 */

use frontend\modules\faq\models\FaqRequestForm;

?>
<?= $this->render('_form', ['model' => $model]) ?>
