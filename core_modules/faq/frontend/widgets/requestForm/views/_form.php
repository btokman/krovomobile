<?php
/**
 * @var \yii\web\View $this
 * @var FaqRequestForm $model
 */

use frontend\modules\faq\models\FaqRequestForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var ActiveForm $form */
$form = ActiveForm::begin([
    'action' => FaqRequestForm::getPagesUrl(),
    'id' => 'faq-form',
    'options' => ['class' => 'form-horizontal ajax-form faq-form'],
]);
?>
<?= $form->field($model, 'category_id')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'name')->textInput(); ?>
<?= $form->field($model, 'email')->textInput(); ?>
<?= $form->field($model, 'phone')->textInput(); ?>
<?= $form->field($model, 'question')->textarea(); ?>
<div class="form-group">
    <div class="col-lg-12">
        <?= Html::submitButton(\Yii::t('front/faq', 'Send'), ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>
