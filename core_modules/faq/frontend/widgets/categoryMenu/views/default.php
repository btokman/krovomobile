<?php
/**
 * @var \yii\web\View $this
 * @var array $items
 */

use yii\widgets\Menu;

echo Menu::widget(['items' => $items, 'options' => ['class' => 'nav nav-pills nav-stacked']]);
?>
