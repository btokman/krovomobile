<?php

namespace frontend\modules\faq\models;

/**
 * Class FaqQuestion
 *
 * @package frontend\modules\faq\models
 */
class FaqQuestion extends \common\models\FaqQuestion
{
    /**
     * @return FaqQuery
     */
    public static function find()
    {
        return new FaqQuery(get_called_class());
    }
}
