<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

use frontend\modules\faq\widgets\categoryMenu\CategoryMenuWidget;
use frontend\modules\faq\widgets\requestForm\RequestFormWidget;
use yii\widgets\ListView;

?>
<div class="col-lg-3">
    <?= CategoryMenuWidget::widget() ?>
</div>
<div class="col-lg-9">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'layout' => "{items}\n{pager}"
    ]) ?>

    <?= RequestFormWidget::widget() ?>
</div>
