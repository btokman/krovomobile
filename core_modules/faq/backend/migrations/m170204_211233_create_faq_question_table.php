<?php

use console\components\Migration;

/**
 * Class m170204_211233_create_faq_question_table migration
 */
class m170204_211233_create_faq_question_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%faq_question}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'category_id' => $this->integer()->null()->comment('Category'),
                'label' => $this->string()->notNull()->comment('Label'),
                'question' => $this->text()->null()->comment('Question'),
                'answer' => $this->text()->null()->comment('Answer'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-faq_question-category_id-faq_category-id',
            $this->tableName,
            'category_id',
            '{{%faq_category}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-faq_question-category_id-faq_category-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
