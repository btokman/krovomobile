<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%contact_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $address
 */
class ContactTranslation extends ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contact_translation}}';
    }
}
