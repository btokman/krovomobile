<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%news_to_news_category}}".
 *
 * @property integer $id
 * @property integer $news_id
 * @property integer $category_id
 *
 * @property NewsCategory $category
 * @property News $news
 */
class NewsToNewsCategory extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_to_news_category}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'News Id',
            'category_id' => 'Category Id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
