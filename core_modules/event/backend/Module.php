<?php

namespace backend\modules\event;

use yii\base\Module as BaseModule;
use common\components\interfaces\CoreModuleBackendInterface;

/**
 * Class Module
 *
 * @package backend\modules\event
 * @author Bogdan Fedun <delagics@gmail.com>
 */
class Module extends BaseModule implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\event\controllers';
}
