<?php

namespace backend\modules\event\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%event_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $content
 * @property string $description
 * @property string $place_label
 */
class EventTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/blog', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/blog', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/blog', 'Label') . ' [' . $this->language . ']',
            'content' => Yii::t('back/blog', 'Content') . ' [' . $this->language . ']',
            'description' => Yii::t('back/blog', 'Description') . ' [' . $this->language . ']',
            'place_label' => Yii::t('back/blog', 'Place label') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'description'], 'string'],
            [['label', 'place_label'], 'string', 'max' => 255],
        ];
    }
}
