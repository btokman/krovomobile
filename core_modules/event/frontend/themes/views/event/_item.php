<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model \frontend\modules\event\models\Event the data model
 * @var $key mixed the key value associated with the data item
 * @var $index integer, the zero-based index of the data item in the items array returned by [[dataProvider]].
 * @var $widget yii\widgets\ListView, this widget instance
 */
?>

<div class="blog-item-inner">
    <h3><?= Html::a($model->label, $model->getViewUrl()) ?></h3>
    <?= $model->getIndexImage('admin', 'file', 'image', ['alt' => 'Do not forget about ALT attr']) ?>
    <div><?= $model->description ?></div>
</div>
