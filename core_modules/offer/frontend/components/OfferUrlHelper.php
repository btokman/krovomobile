<?php

namespace frontend\modules\offer\components;

use common\components\model\Helper;

class OfferUrlHelper
{
    use Helper;

    /**
     * Method returns url for offer category view url for given parameters
     * Parameters must be an array like this ['categoryAlias' => 'newYearCategory']
     *
     * @param array $params
     *
     * @return string
     */
    public static function getOfferCategoryUrl(array $params)
    {
        return static::createUrl(static::getOfferCategoryRoute(), $params);
    }

    /**
     * Method returns route for offer category view url
     *
     * @return string
     */
    public static function getOfferCategoryRoute()
    {
        return '/offer/offer/view-category';
    }


    /**
     * Method returns url for offer view url for given parameters
     * Parameters must be an array like this ['offerAlias' => 'newYearOffer']
     *
     * @param array $params
     *
     * @return string
     */
    public static function getOfferUrl(array $params)
    {
        return static::createUrl(static::getOfferRoute(), $params);
    }

    /**
     * Method returns route for offer view url
     *
     * @return string
     */
    public static function getOfferRoute()
    {
        return '/offer/offer/view-offer';
    }
}