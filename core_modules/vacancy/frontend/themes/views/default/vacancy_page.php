<?php
/**
 * Created by anatolii
 *
 * @var $this \yii\web\View
 * @var $model Vacancy
 * @var $requestForm VacancyRequestForm
 */
use common\models\Vacancy;
use frontend\modules\vacancy\models\VacancyRequestForm;
use metalguardian\fileProcessor\helpers\FPM;

?>
<h1>
    <?= $model->label ?>
</h1>
<?php foreach ($model->images as $image): ?>
    <div class="slide-img">
        <img src="<?= FPM::originalSrc($image->file_id) ?>">
    </div>
<?php endforeach; ?>
<h3>Description</h3>
<p><?= $model->description ?></p>
<h3>Responsibilities</h3>
<p><?= $model->responsibilities ?></p>
<h3>Qualifications</h3>
<p><?= $model->qualifications ?></p>

<?= $this->render('_request_form', ['model' => $requestForm]) ?>
