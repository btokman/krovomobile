<?php

use console\components\Migration;

/**
 * Class m170124_091956_create_vacancy_table_translation migration
 */
class m170124_091956_create_vacancy_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%vacancy_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%vacancy}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
                'description' => $this->text()->comment('Description'),
                'responsibilities' => $this->text()->comment('Responsibilities'),
                'qualifications' => $this->text()->comment('Qualifications'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-vacancy_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-vacancy_translation-model_id-vacancy-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

