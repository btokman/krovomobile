<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%vacancy_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $description
 * @property string $responsibilities
 * @property string $qualifications
 * @property EntityToFile[] $images
 */
class VacancyTranslation extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_IMAGES = 'VacancyTranslationImages';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vacancy_translation}}';
    }
}
