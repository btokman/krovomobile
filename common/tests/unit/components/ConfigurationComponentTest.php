<?php
namespace common\tests\unit\components;

use Yii;
use common\tests\base\unit\DbTestCase;
use common\tests\fixtures\ConfigurationFixture;
use common\tests\fixtures\ConfigurationTranslationFixture;
use common\models\Configuration;
use common\models\ConfigurationTranslation;
use common\components\ConfigurationComponent;

/**
 * Test case for [[\common\components\ConfigurationComponent]]
 * @see \common\components\ConfigurationComponent
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class ConfigurationComponentTest extends DbTestCase
{
    /**
     * @var \common\components\ConfigurationComponent
     */
    private $config;


    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'configuration' => ['class' => ConfigurationFixture::className()],
            'configurationTranslation' => ['class' => ConfigurationTranslationFixture::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();
        $this->config = Yii::$app->get('config');
    }

    public function testInitException()
    {
        $this->expectException('yii\base\InvalidConfigException');
        new ConfigurationComponent(['defaultLanguage' => null]);
    }

    public function testInit()
    {
        $closure = function () {
            return 'uk';
        };
        $config = new ConfigurationComponent(['defaultLanguage' => $closure]);

        $this->assertEquals($config->defaultLanguage, call_user_func($closure));
    }

    public function testGetDefaultLanguage()
    {
        $key = 'test-1';
        $expected = Configuration::findOne($key)->value;
        $actual = $this->config->get($key);

        $this->assertEquals($expected, $actual);
    }

    public function testGetCurrentLanguage()
    {
        $key = 'test-1';
        $language = 'uk';
        $expected = ConfigurationTranslation::find()
            ->select('value')
            ->where([
                'model_id' => $key,
                'language' => $language
            ])
            ->scalar();
        $actual = $this->config->get($key, null, $language);

        $this->assertEquals($expected, $actual);
    }

    public function testGetDefaultValue()
    {
        $this->assertNull($this->config->get('not exists key'), null);
    }

    public function testHas()
    {
        $this->config->set('test-1');
        $this->assertTrue($this->config->has('test-1'));
        $this->assertFalse($this->config->has('not exists key'));
    }

    public function testSet()
    {
        $actual = $this->config->set('test-3');
        $this->assertTrue($actual);
        $this->assertFalse($this->config->set('not exists key'));
    }

    public function testLoadConfigsByKeys()
    {
        $this->config->loadConfigsByKeys('test-4');
        $this->assertTrue($this->config->has('test-4'));
    }
}
