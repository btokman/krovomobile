<?php
namespace common\tests\base\unit;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\di\Container;
use Codeception\Specify;

/**
 * Base test case for unit tests.
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class TestCase extends \Codeception\TestCase\Test
{
    use Specify;

    /**
     * @var string
     */
    public $appConfig = '@tests/_config/app.php';

    /**
     * @var \UnitTester
     */
    protected $tester;


    /**
     * Init method calls in setUp()
     */
    protected function init()
    {

    }

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();
        $this->mockApplication();
        $this->init();
    }

    /**
     * @inheritdoc
     */
    protected function tearDown()
    {
        $this->destroyApplication();
        parent::tearDown();
    }

    /**
     * Mocks up the application instance
     * @param array $config the configuration that should be used to generate the application instance
     * If null, [[appConfig]] option will be used
     * @return \yii\web\Application|\yii\console\Application the application instance
     * @throws InvalidConfigException if the application configuration is invalid
     */
    protected function mockApplication($config = null)
    {
        Yii::$container = new Container();
        $config = $config ?: $this->appConfig;
        if (is_string($config)) {
            $configFile = Yii::getAlias($config);
            if (!is_file($configFile)) {
                throw new InvalidConfigException("The application configuration file does not exist: $config");
            }
            $config = require($configFile);
        }
        if (is_array($config)) {
            if (!isset($config['class'])) {
                $config['class'] = 'yii\web\Application';
            }
            return Yii::createObject($config);
        }

        throw new InvalidConfigException('Please provide a configuration array to mock up an application.');
    }

    /**
     * Destroys the application instance
     */
    protected function destroyApplication()
    {
        if (Yii::$app) {
            if (Yii::$app->has('session', true)) {
                Yii::$app->session->close();
            }
            if (Yii::$app->has('db', true)) {
                Yii::$app->db->close();
            }
        }
        Yii::$app = null;
        Yii::$container = new Container();
    }

    /**
     * Assert that model is valid
     *
     * If model does not valid - model errors will be
     * displayed in test error message
     *
     * @param Model $model
     */
    public function assertModelValid($model)
    {
        if ($model instanceof Model) {
            $this->assertTrue(
                $model->validate(),
                'Validation failed: ' . print_r($model->getErrors(), true)
            );
        }
        else {
            throw \PHPUnit_Util_InvalidArgumentHelper::factory(1, Model::className());
        }
    }

    /**
     * Assert that string contains a substring
     *
     * @param string $string
     * @param string|array $subString
     * @param string $message
     */
    public function assertStringContains($string, $subString, $message = '')
    {
        if (is_array($subString)) {
            foreach ($subString as $str) {
                $this->assertTrue(strpos($string, $str) !== false, $message);
            }
        }
        else {
            $this->assertTrue(strpos($string, $subString) !== false, $message);
        }
    }
}
