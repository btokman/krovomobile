<?php
namespace common\tests\fixtures;

use yii\test\ActiveFixture;

/**
 * Fixture for [[\common\models\EntityToFile]] model
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class EntityToFileFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\EntityToFile';
    /**
     * @inheritdoc
     */
    public $dataFile = '@common/tests/_data/entity-to-file.php';
}
