<?php
return [
    'fileProcessor' => [
        'class' => \metalguardian\fileProcessor\Module::className(),
        'baseUploadDirectory' => '@tests/_output/',
        'originalDirectory' => 'uploads'
    ],
];
