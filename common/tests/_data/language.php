<?php

return [
    [
        'id'            => 1,
        'label'         => 'English',
        'code'          => 'en',
        'locale'        => 'en-US',
        'published'     => true,
        'position'      => 1,
        'is_default'    => true,
    ],
    [
        'id'            => 2,
        'label'         => 'Ukrainian',
        'code'          => 'uk',
        'locale'        => 'uk-UA',
        'published'     => true,
        'position'      => 2,
        'is_default'    => false,
    ],
    [
        'id'            => 3,
        'label'         => 'Russian',
        'code'          => 'ru',
        'locale'        => 'ru-RU',
        'published'     => true,
        'position'      => 3,
        'is_default'    => false,
    ],
];
