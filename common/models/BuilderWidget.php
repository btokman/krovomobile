<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%builder_widget}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $target_class
 * @property integer $target_id
 * @property string $target_sign
 * @property string $target_attribute
 * @property string $widget_class
 * @property integer $position
 *
 * @property BuilderWidgetAttribute[] $builderWidgetAttributes
 * @property EntityToFile[] $entityToFile
 */
class BuilderWidget extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%builder_widget}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilderWidgetAttributes()
    {
        return $this->hasMany(BuilderWidgetAttribute::className(), ['widget_id' => 'id']);
    }
}
