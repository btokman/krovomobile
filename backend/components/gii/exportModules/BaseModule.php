<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules;

use yii\base\Object;


/**
 * Class BaseModule
 *
 * @package backend\components\gii\exportModules
 */
abstract class BaseModule extends Object
{
    /**
     * BaseModule constructor.
     *
     * @param $moduleId
     */
    public function __construct($moduleId)
    {
        parent::__construct();

        $this->id = $moduleId;
    }

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $backend;

    /**
     * @var string
     */
    public $frontend;

    /**
     * @var string
     */
    public $common;

    /**
     * @var array
     */
    public $log = [];

    /**
     * @return string
     */
    abstract public function getBackendPath();

    /**
     * @return string
     */
    abstract public function getFrontendPath();
    
    /**
     * @return string
     */
    abstract public function getFrontendThemesPath();

    /**
     * @return string
     */
    abstract public function getCommonPath();

    /**
     * @param $path
     * @param array $paths
     *
     * @return array
     */
    protected function getFilesFromDirectory($path, &$paths = [])
    {
        $files = glob($path . '/*');
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->getFilesFromDirectory($file, $paths);
            } else {
                $paths[] = $file;
            }
        }

        return $paths;
    }

    /**
     * @param string $path
     */
    protected function removeDirectory($path)
    {
        $files = glob($path . '/*');
        foreach ($files as $file) {
            is_dir($file) ? $this->removeDirectory($file) : unlink($file);
            $this->log[] = "$file was removed";
        }
        if (is_dir($path)) {
            rmdir($path);
            $this->log[] = "$path was removed";
        }
    }
}
