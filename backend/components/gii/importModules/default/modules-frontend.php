<?php
/**
 * @var $modules array
 */

echo "<?php\n";
?>

return [
<?php foreach ($modules as $moduleId): ?>
    '<?= $moduleId ?>' => [
        'class' => 'frontend\modules\<?= $moduleId ?>\Module',
    ],
<?php endforeach; ?>
];
