<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\importModules;

use backend\components\gii\exportModules\BaseModule;
use backend\components\gii\exportModules\AppModule;
use Yii;


/**
 * Class CoreModule
 *
 * @package backend\components\gii\exportModules
 */
class CoreModule extends BaseModule
{
    /**
     * @var AppModule
     */
    private $_appModule;

    /**
     * CoreModule constructor.
     *
     * @param $moduleId
     */
    public function __construct($moduleId)
    {
        parent::__construct($moduleId);
    }

    /**
     * @return AppModule
     */
    protected function getAppModule()
    {
        if (!$this->_appModule) {
            $this->_appModule = new AppModule($this->id);
        }

        return $this->_appModule;
    }

    /**
     * @return string
     */
    public function getRootPath()
    {
        return Yii::getAlias('@root') . '/core_modules/' . $this->id;
    }

    /**
     * @return string
     */
    public function getBackendPath()
    {
        return $this->getRootPath() . '/backend';
    }

    /**
     * @return string
     */
    public function getFrontendPath()
    {
        return $this->getRootPath() . '/frontend';
    }

    /**
     * @return string
     */
    public function getFrontendThemesPath()
    {
        return $this->getRootPath() . '/frontend/themes';
    }

    /**
     * @return string
     */
    public function getCommonPath()
    {
        return $this->getRootPath() . '/common';
    }

    /**
     * @return array
     */
    public function getFilesForImport()
    {
        $appModule = $this->getAppModule();
        $files = [];
        foreach ($this->getFilesFromDirectory($this->getBackendPath()) as $item) {
            $array = explode("$this->id/backend", $item);
            if (isset($array[1])) {
                $files[$item] = $appModule->getBackendPath() . $array[1];
            }
        }
        foreach ($this->getFilesFromDirectory($this->getFrontendPath()) as $item) {
            $array = explode("$this->id/frontend", $item);
            if (isset($array[1]) && !substr_count('frontend/themes', $item)) {
                $files[$item] = $appModule->getFrontendPath() . $array[1];
            }
        }
        foreach ($this->getFilesFromDirectory($this->getFrontendThemesPath()) as $item) {
            $array = explode("$this->id/frontend/themes", $item);
            if (isset($array[1])) {
                $files[$item] = $appModule->getFrontendThemesPath() . $array[1];
            }
        }
        foreach ($this->getFilesFromDirectory($this->getCommonPath()) as $item) {
            $array = explode("$this->id/common", $item);
            if (isset($array[1])) {
                $files[$item] = $appModule->getCommonPath() . $array[1];
            }
        }

        return $files;
    }
}
