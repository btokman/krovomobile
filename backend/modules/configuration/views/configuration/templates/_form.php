<?php

use backend\modules\configuration\components\ConfigurationFormBuilder;
use common\components\model\Translateable;
use common\helpers\LanguageHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\modules\configuration\components\ConfigurationModel */

$values = $model->getModels();
?>

<div class="menu-form">
    <?= Html::errorSummary(
        $values,
        [
            'class' => 'alert alert-danger'
        ]
    );
    ?>
    <?php /** @var ConfigurationFormBuilder $form */
    $form = ConfigurationFormBuilder::begin([
        'enableClientValidation' => false,
        'options' => [
            'id' => 'main-form',
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?php
    $items = [];

    $content = null;
    foreach ($values as $value) {
        $language = $value instanceof Translateable ? '[language: ' . LanguageHelper::getCurrent()->code . ']' : null;
        $attribute = '[' . $value->id . ']value';
        $configuration = $value->getValueFieldConfig();
        $configuration['label'] = $model->showAsConfig ?
            $value->description . ' [key: ' . $value->id . ']' . $language
            : $value->description;
        $content .= $form->renderField($value, $attribute, $configuration);
        $content .= $form->renderUploadedFile($value, 'value', $configuration);
        if (!is_array($value->type) && $language && $model->isTranslateAttribute($value->id)) {
            foreach ($value->getTranslationModels() as $languageModel) {
                $configuration['label'] = $model->showAsConfig ?
                    $value->description . ' [key: ' . $value->id . '] [language: ' . $languageModel->language . ']' :
                    $value->description . ' [' . $languageModel->language . ']';
                $content .= $form->renderField($languageModel, '[' . $languageModel->language . ']' . $attribute,
                    $configuration);
                $content .= $form->renderUploadedFile($languageModel, 'value', $configuration,
                    $languageModel->language);
            }
        }
    }

    $translationModels = [];
    if ($model instanceof Translateable) {
        $translationModels = $model->getTranslationModels();
    }
    $formConfig = $model->getFormConfig();
    $tabs = [];
    if (isset($formConfig['form-set'])) {
        $i = 0;
        foreach ($formConfig['form-set'] as $tabName => $tabConfig) {
            if (!$tabName) {
                $content .= $form->prepareRows($model, $tabConfig, $translationModels);
            } else {
                $class = 'tab_content_' . ++$i;
                $tabs[] = [
                    'label' => $tabName,
                    'content' => $form->prepareRows($model, $tabConfig, $translationModels, true),
                    'options' => [
                        'class' => $class,
                    ],
                    'linkOptions' => [
                        'class' => $class,
                    ],
                ];
            }
        }
    } elseif ($formConfig) {
        $content .= $form->prepareRows($model, $formConfig, $translationModels);
    }

    if (!is_null($content)) {
        $items[] = [
            'label' => 'Content',
            'content' => $content,
            'active' => true,
            'options' => [
                'class' => 'tab_content_content',
            ],
            'linkOptions' => [
                'class' => 'tab_content',
            ],
        ];
    }
    $items = ArrayHelper::merge($items, $tabs);

    $seo = $model->getBehavior('seo');
    if ($seo && $seo instanceof \notgosu\yii2\modules\metaTag\components\MetaTagBehavior) {
        $seo = \notgosu\yii2\modules\metaTag\widgets\metaTagForm\Widget::widget(['model' => $model,]);
        $items[] = [
            'label' => 'SEO',
            'content' => $seo,
            'active' => (is_null($content)) ? true : false,
            'options' => [
                'class' => 'tab_seo_content',
            ],
            'linkOptions' => [
                'class' => 'tab_seo',
            ],
        ];
    }
    ?>

    <?= \yii\bootstrap\Tabs::widget(['items' => $items]) ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']); ?>
            </div>
        </div>
    </div>

    <?php ConfigurationFormBuilder::end(); ?>

</div>
