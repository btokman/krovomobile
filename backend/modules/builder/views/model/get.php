<?php
/**
 * @var \yii\web\View $this
 * @var \common\components\model\ActiveRecord $model
 * @var integer $id
 * @var string $attribute
 * @var \common\components\BuilderModel $widget
 * @var int $key
 */

use yii\helpers\Html;
use backend\modules\builder\components\DummyFormBuilder;
use yii\helpers\ArrayHelper;

$name = $widget::className();
$fields = $widget->getConfig();
$localized = $widget->getLocalized();
$locales = \common\helpers\LanguageHelper::getApplicationLanguages();
$defaultLocale = \common\helpers\LanguageHelper::getDefaultLanguage();
$defaultLocale = \yii\helpers\ArrayHelper::getValue($defaultLocale, 'locale');

/** @var DummyFormBuilder $form */
$form = DummyFormBuilder::begin();

echo \yii\helpers\Html::beginTag('div', [
    'class' => (Yii::$app->request->isAjax) ? 'builder-row panel' : 'builder-row ui-sortable-handle panel'
]);

echo Html::beginTag('div', ['class' => 'row-header clearfix']);

echo Html::a("<i class='glyphicon glyphicon-move'></i>", "#",
    ['class' => 'sub-btn content-row-trigger-sort']);

echo Html::tag('span', $widget->getName(), ['class' => '']);

echo Html::a("<i class='glyphicon glyphicon-trash'></i>", "#",
    ['class' => 'sub-btn content-row-trash']);

echo Html::a("<i class='glyphicon glyphicon-resize-vertical'></i>", "#",
    ['class' => 'sub-btn content-row-sort']);

echo Html::endTag('div');


echo Html::beginTag('div', ['class' => 'content-form']);

foreach ($fields as $attr => $config) {
    if (in_array($attr, $localized)) {
        foreach ($locales as $locale) {
            $tmpAttr = $attr;
            if ($locale == $defaultLocale) {
                echo $form->renderField($widget, "[{$key}]{$attr}", $config);
            } else {
                $tmpAttr = $attr . "_{$locale}";
                echo $form->renderField($widget, "[{$key}]{$tmpAttr}", $config);
            }
        }
    } else {
        echo $form->renderField($widget, "[{$key}]{$attr}", $config);
    }
}

echo Html::endTag('div');

DummyFormBuilder::end();

echo \yii\helpers\Html::endTag('div');
