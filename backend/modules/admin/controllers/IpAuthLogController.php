<?php

namespace backend\modules\admin\controllers;

use backend\components\BackendController;
use backend\modules\admin\models\IpAuthLog;

/**
 * IpAuthLogController implements the CRUD actions for IpAuthLog model.
 */
class IpAuthLogController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return IpAuthLog::className();
    }
}
