<?php
namespace backend\tests\fake\models;

use common\components\model\ActiveRecord;

/**
 * Active record model for `test_relation_table`
 *
 * @property integer $id
 * @property integer $rel_table_id
 * @property string $test_data_field
 * @property string $language
 *
 * @property TestTable $testTable
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class TestRelationTable extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_relation_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rel_table_id'], 'required'],
            [['rel_table_id'], 'integer'],

            [['test_data_field'], 'string', 'max' => 255],

            [['language'], 'string'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestTable()
    {
        return $this->hasOne(TestTable::tableName(), ['rel_table_id' => 'id']);
    }
}
