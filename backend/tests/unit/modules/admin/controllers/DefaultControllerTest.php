<?php
namespace backend\tests\unit\modules\admin\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Response;
use common\tests\base\unit\DbTestCase;
use common\tests\fixtures\UserFixture;
use common\components\UserIdentity;
use common\models\User;
use backend\modules\admin\controllers\DefaultController;

/**
 * Test case for [[\backend\modules\admin\controllers\DefaultController]]
 * @see \backend\modules\admin\controllers\DefaultController
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class DefaultControllerTest extends DbTestCase
{
    /**
     * @var DefaultController
     */
    private $controller;


    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'user' => ['class' => UserFixture::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->controller = new DefaultController(null, null);
        $this->controller->setViewPath('@tests/fake/views');
    }

    /**
     * Simulate user login
     */
    private function login()
    {
        $entity = User::findOne(['email' => 'tester@test.test']);
        $identity = new UserIdentity($entity);
        Yii::$app->getUser()->login($identity);
    }

    /**
     * Example behaviors config
     *
     * @return array
     */
    private function getBehavioursConfig()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'auth'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function testBehaviorsConfig()
    {
        $this->specify(
            'access filter',
            function () {
                $expected = $this->getBehavioursConfig()['access'];
                $actual = $this->controller->behaviors();

                $this->assertTrue(isset($actual['access']));
                $this->assertEquals($expected, $actual['access']);
            }
        );

        $this->specify(
            'verb filter',
            function () {
                $expected = $this->getBehavioursConfig()['verbs'];
                $actual = $this->controller->behaviors();

                $this->assertTrue(isset($actual['verbs']));
                $this->assertEquals($expected, $actual['verbs']);
            }
        );
    }

    public function testActionLogin()
    {
        $this->specify(
            'not authenticated user error',
            function () {
                Yii::$app->getRequest()->setData([
                    'LoginForm' => [
                        'email' => 'wrong email',
                        'password' => 'wrong password'
                    ],
                ]);
                $response = $this->controller->actionLogin();

                $this->assertInternalType('string', $response);
            }
        );

        $this->specify(
            'not authenticated user success',
            function () {
                Yii::$app->getRequest()->setData([
                    'LoginForm' => [
                        'email' => 'tester@test.test',
                        'password' => 'test'
                    ],
                ]);
                $response = $this->controller->actionLogin();

                $this->assertInstanceOf(Response::className(), $response);
                $this->assertEquals(
                    Url::to(Yii::$app->getUser()->getReturnUrl(), true),
                    $response->headers['location']
                );
            }
        );

        $this->specify(
            'authenticated user',
            function () {
                $this->login();
                $response = $this->controller->actionLogin();

                $this->assertInstanceOf(Response::className(), $response);
                $this->assertEquals(
                    Url::to(Yii::$app->getHomeUrl(), true),
                    $response->headers['location']
                );
            }
        );
    }

    public function testActionLogout()
    {
        $this->login();
        $response = $this->controller->actionLogout();

        $this->assertInstanceOf(Response::className(), $response);
        $this->assertEquals(
            Url::to(Yii::$app->getHomeUrl(), true),
            $response->headers['location']
        );
        $this->assertTrue(Yii::$app->getUser()->getIsGuest());
    }
}
