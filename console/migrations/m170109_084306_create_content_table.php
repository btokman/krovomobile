<?php

use console\components\Migration;

/**
 * Class m170109_084306_create_content_table migration
 */
class m170109_084306_create_content_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%content}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'target_class' => $this->string()->notNull()->comment('Model Class'),
                'target_id' => $this->integer()->notNull()->comment('Model ID'),
                'target_attribute' => $this->string()->notNull()->comment('Model Attribute'),
                'sign' => $this->string()->notNull()->comment('Sign'),
                'name' => $this->string()->notNull()->comment('Name'),
                'attribute' => $this->string()->notNull()->comment('Attribute'),
                'value' => $this->text()->null()->comment('Value'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
