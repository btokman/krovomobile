<?php

use console\components\Migration;

/**
 * Class m170109_084306_create_content_table_translation migration
 */
class m170109_084306_create_content_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%content_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%content}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'value' => $this->text()->comment('Value'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-content_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-content_translation-model_id-content-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

