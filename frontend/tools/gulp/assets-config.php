<?php
/*
Yii command:
yii asset tools/gulp/assets-config.php config/assets-prod.php
*/

Yii::setAlias('@webroot', str_replace('\\', '/', dirname(__FILE__)) . '/../../web');
Yii::setAlias('@web', '/');

return [
    'jsCompressor' => 'gulp compress-js --gulpfile frontend/tools/gulp/gulpfile.js --src {from} --dist {to}',
    'cssCompressor' => 'gulp compress-css --gulpfile frontend/tools/gulp/gulpfile.js --src {from} --dist {to}',
    'bundles' => [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\widgets\ActiveFormAsset',
        'yii\validators\ValidationAsset',
        'frontend\assets\AppAsset',
        'frontend\assets\AppAsset',
    ],
    'targets' => [
        'common' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/compress',
            'baseUrl' => '@web/compress',
            'js' => 'common-' . time() . '.js',
            'css' => 'common-' . time() . '.css',
            'depends' => [
                'yii\web\JqueryAsset',
                'yii\web\YiiAsset',
                'yii\bootstrap\BootstrapAsset',
                'yii\widgets\ActiveFormAsset',
                'yii\validators\ValidationAsset',
            ],
        ],
        'all' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/compress',
            'baseUrl' => '@web/compress',
            'js' => 'all-' . time() . '.js',
            'css' => 'all-' . time() . '.css',
            'depends' => [
                'frontend\assets\AppAsset',
            ],
        ],
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets',
    ],
];
