<div id="wrapper">
    <a href="http://krovomobile.org/" target="_blank">
        <div id="logo">
            <img src="img/logo.png">
        </div>
    </a>
    <div class="left_section">
        <div class="inner_text">
            <div></div>
            <span>Поддержать проект</span></div>
        <div class="about_inner">
            <h1>"Кровомобиль"</h1>
            <div>
                — это национальный проект «Медицины Катастроф», автопарк транспортных средств в зоне АТО и на всей
                территории Украины, которое доставляет кровь и её компоненты, плазму, медикаменты, медицинское
                оборудование для предоставления неотложной медицинской догоспитальной помощи тяжело раненым людям.
            </div>
            <div class="image_inner">
                <div></div>
                <img src="img/car.jpg">
            </div>

        </div>
    </div>
    <div class="right_section">
        <div class="inner_text">
            <div></div>
            <span>Каждый может помочь нам спасать жизни</span>
        </div>
        <div class="inner_buttons">
            <div class="inner_button">50 UAH</div>
            <div class="inner_button">200 UAH</div>
            <div class="inner_button">600 UAH</div>
            <div class="inner_button">1000 UAH</div>
        </div>
        <div class="inner_form">
            <div class="inner_text">
                <div></div>
                <span>Другая сумма</span></div>
            <input placeholder="UAH" type="text">
            <div class="button">Поддержать</div>
        </div>
    </div>

</div>
